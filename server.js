const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const server = express();

server.use(logger('dev'));
server.use(cookieParser());
server.use(express.static(path.join(__dirname, '/public')));
server.use(express.static(path.join(__dirname, '/node_modules')));

// since the app is single-page, send all requests to the index file;
// the app's router will route them to where they should go
server.use(
    function(req, res) {
        res.sendFile(path.join(__dirname, '/public/index.html'));
    });

// catch 404 and forward to error handler
server.use(
    function(req, res, next) {
        const err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

// error handlers

// development error handler
// will print stacktrace
if (server.get('env') === 'development') {
  server.use(function(err, req, res) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
server.use(function(err, req, res) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

// start the HTTP server
const http = require('http');
const port = 80;
http.createServer(server)
    .listen(
        port,
        (err) => {
            if (err) console.error(err.message);
            else console.log("API listening on port " + port)
        }
    );
