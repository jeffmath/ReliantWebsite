This website employs React, react-router, and Bootstrap 3.

To build this project, run the webpack batch file from a terminal in the
project's root folder.  Webpack produces a bundle.js file in the "public" folder
on success.  With the package-lock.json file present, there shouldn't be any 
build errors due to incompatible package-dependency versions.
 
This project contains a simple web server in the server.js file which may be
used to support local development.  To run it, create a configuration for that
file in Intellij IDEA, and run that configuration.  Note that it is setup to
serve files from the node_modules folder when necessary, such as for the
bootstrap CSS file referenced in index.html.

To deploy this website, upload those files in the public folder which have
changed, which should usually be just bundle.js.