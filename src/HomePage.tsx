import * as React from 'react';
import "../node_modules/react-image-gallery/styles/css/image-gallery.css";
import {PortfolioCarousel} from "./portfolio/PortfolioCarousel";

export const HomePage = () =>
    <div className="home-page side-margins-40px">
        <h1 className="Alegreya">
            Reliant Software Engineering, LLC
        </h1>
        <h4><i>Your affordable custom programming shop</i></h4>
        <section>
            <div className="row">
                <div className="col-md-6">
                    <h3>What we do</h3>
                    <ul>
                        <li>Data-driven web applications</li>
                        <li>Web backend/API programming</li>
                        <li>Database design and reporting</li>
                        <li>Mobile app development</li>
                        <li>Desktop program creation</li>
                    </ul>
                    <h3>Our portfolio</h3>
                    <ul>
                        <li><a href="/#/mapsIntegration">Maps integration</a></li>
                        <li><a href="/#/businessAutomation">Business automation</a></li>
                        <li><a href="/#/onlineStore">Online store</a></li>
                        <li><a href="/#/dataGathering">Data gathering</a></li>
                        <li><a href="/#/orderManagement">Order management</a></li>
                        <li><a href="/#/productCatalog">Product catalog</a></li>
                        <li><a href="/#/mobileApp">Mobile app</a></li>
                        <li><a href="/#/forFun">For fun</a></li>
                    </ul>
                </div>
                <div className="col-md-6">
                    <PortfolioCarousel/>
                </div>
            </div>
        </section>
        <section>
            <h3>Why hire us?</h3>
            <a href="/#/contact">
                <div className="call-us-sign">
                    Call us for our<br/>current low rate!
                </div>
            </a>
            <p>
                One low rate for our services provides you an American programmer with
                over twenty years of software development experience, who also holds a
                Master's Degree in Computer Engineering.
            </p>
            <p>
                Through conversations we have with you, we formulate the business
                goals of your proposed custom software.  We then help you prioritize
                those goals, and begin addressing them in that order.  For each goal,
                we determine the minimum functionality which will achieve it,
                and implement only that functionality.  We also reuse existing
                code from past projects wherever possible, to save time and money.
            </p>
            <p>
                We aim to be your lowest cost solution for implementing quality
                custom software, utilizing the most efficient and maintainable
                programming technologies and methodologies.
            </p>
        </section>
        <section>
            <h3>How we collaborate with you</h3>
            <p>
                As we are located in Tucson, Arizona, we perform our work for our clients
                remotely.  We utilize desktop sharing programs simultaneously with phone calls
                to enable highly productive remote collaboration.  We take pride in making
                ourselves available to collaborate with customers seven days a week,
                over a fifteen hour range each day.
            </p>
            <p>
                We are here to empower your business by delivering affordable custom
                software which meets your specific needs.
            </p>
        </section>
    </div>;
