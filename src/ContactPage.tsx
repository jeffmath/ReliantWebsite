import * as React from 'react'

export const ContactPage = () =>
    <div className="row vertical-center">
        <div className="col-md-6">
            <h2>Contact Information</h2>
            <p>
                <b>Jeff Mather</b><br/>
                <i>Principal Software Engineer</i><br/>
                520-293-9066<br/>
                9 AM to midnight (Arizona time), 7 days a week<br/>
                <a href="mailto:jeff@reliantsoftwareeng.com">Or, send me an e-mail</a>
            </p>
            <p>
                Over twenty years of successful software development experience.
            </p>
            <p>
                M.S., B.Sc. in Computer Engineering<br/>
                (both with a software engineering emphasis)<br/>
                from the University of Arizona
            </p>
            <p>
                Jeff has completed projects meeting the standards of such large entities as:
            </p>
            <ul>
                <li>Kroll Bond Rating Agency</li>
                <li>Nuvasive</li>
                <li>The University of Arizona</li>
                <li>The U.S. Naval Research Laboratory</li>
                <li>Unisource Energy</li>
                <li>The City of Tucson</li>
                <li>Mentor Graphics</li>
            </ul>
            <p>
                as well as projects for many smaller companies and organizations.
            </p>
            <p><i>I look forward to speaking with you.</i></p>
        </div>
        <div className="col-md-6 margin-top-20px">
            <img className="dragons-image" src="/images/two_dragons_small.jpg"/>
        </div>
    </div>;
