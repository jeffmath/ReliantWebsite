import * as React from 'react'
import {HashRouter, Route} from 'react-router-dom';
import {App} from "./App";

export const router =
    <HashRouter>
        <div>
            <Route path="/" component={App}/>
        </div>
    </HashRouter>;
