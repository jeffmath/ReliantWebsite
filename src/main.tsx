import * as React from 'react';
import {render} from 'react-dom';
import {router} from './router';

render(
    router,
    document.getElementById('react-container')
);
