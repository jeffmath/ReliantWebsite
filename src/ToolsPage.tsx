import * as React from 'react'
import {PortfolioCarousel} from "./portfolio/PortfolioCarousel";

export const ToolsPage = () =>
    <div className="tools-page">
        <div className="row vertical-center">
            <div className="col-md-6">
                <PortfolioCarousel/>
            </div>
            <div className="col-md-6">
                <h2>The tools we use</h2>
                <p><i>(In order of preference for each task)</i></p>
                <h4>For web application creation</h4>
                <ul>
                    <li>Angular, using TypeScript</li>
                    <li>React, using TypeScript</li>
                    <li>ASP.NET MVC, using C#</li>
                    <li>For styling: Bootstrap or Material Design</li>
                </ul>
                <h4>For web service/backend creation</h4>
                <ul>
                    <li>Play Framework 2.x, using Scala</li>
                    <li>Node.js with Express.js, using TypeScript</li>
                    <li>ASP.NET Web-API, using C#</li>
                </ul>
                <h4>For Android mobile app creation</h4>
                <ul>
                    <li>Android SDK, using Kotlin</li>
                </ul>
                <h4>For Android + iOS mobile app creation</h4>
                <ul>
                    <li>React Native, using TypeScript</li>
                </ul>
                <h4>For desktop program creation</h4>
                <ul>
                    <li>WPF with Entity Framework, using C#</li>
                    <li>[JavaFX or Java Swing] with Hibernate, using Kotlin</li>
                </ul>
                <h4>For database design and reporting</h4>
                <ul>
                    <li>MySQL or PostgreSQL</li>
                    <li>SQL Server</li>
                    <li>MongoDB</li>
                    <li>Oracle</li>
                </ul>
            </div>
        </div>
        <div>
            <p>This website is a React application.  You can view its <a href="https://gitlab.com/jeffmath/ReliantWebsite">GitLab repository</a>.</p>
        </div>
    </div>;