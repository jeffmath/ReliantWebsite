import * as React from 'react';
import {Navigation} from "./Navigation";
import {Route, Switch} from "react-router-dom";
import {HomePage} from "./HomePage";
import {MapsIntegrationPage} from "./portfolio/MapsIntegrationPage";
import {BusinessAutomationPage} from "./portfolio/BusinessAutomationPage";
import {DataGatheringPage} from "./portfolio/DataGatheringPage";
import {OnlineStorePage} from "./portfolio/OnlineStorePage";
import {OrderManagementPage} from "./portfolio/OrderManagementPage";
import {ProductCatalogPage} from "./portfolio/ProductCatalogPage";
import {MobileAppPage} from "./portfolio/MobileAppPage";
import {ForFunPage} from "./portfolio/ForFunPage";
import {ToolsPage} from "./ToolsPage";
import {ContactPage} from "./ContactPage";

export const App = () =>
    <div className="app">
        <Navigation/>
        <img src='./images/banner.jpg' width="100%" alt="banner" className="banner"/>
        <div className="container padding-bottom-20px" style={{background: 'white'}}>
            <Switch>
                <Route exact path="/" component={HomePage}/>
                <Route path="/mapsIntegration" component={MapsIntegrationPage}/>
                <Route path="/businessAutomation" component={BusinessAutomationPage}/>
                <Route path="/dataGathering" component={DataGatheringPage}/>
                <Route path="/onlineStore" component={OnlineStorePage}/>
                <Route path="/orderManagement" component={OrderManagementPage}/>
                <Route path="/productCatalog" component={ProductCatalogPage}/>
                <Route path="/mobileApp" component={MobileAppPage}/>
                <Route path="/forFun" component={ForFunPage}/>
                <Route path="/tools" component={ToolsPage}/>
                <Route path="/contact" component={ContactPage}/>
            </Switch>
        </div>
        <img src='./images/banner.jpg' width="100%" alt="banner" className="banner flip-x-y"/>
    </div>;
