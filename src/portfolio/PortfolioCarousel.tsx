import * as React from 'react';
const ImageGallery = require('react-image-gallery').default;
import imagePaths from './ImagePaths';

// note that we filter out the Dunjax screenshots from the carousel display
const images =
    Object.keys(imagePaths).filter(key => !key.startsWith("ff"))
        .map((k: string) => {return {original: (imagePaths as any)[k]}}) as Object[];

export const PortfolioCarousel = () =>
    <ImageGallery
        items={images} slideInterval={4000} showThumbnails={false}
        autoPlay={true} showFullscreenButton={false}
    />;
