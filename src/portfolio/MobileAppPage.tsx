import * as React from 'react'
import imagePaths from "./ImagePaths";

export const MobileAppPage = () =>
    <div className="flex">
        <div className="spacer-column"/>
        <div className="content-column">
            <h3>Photo Logo Add app, for Android</h3>
            <p className="limit-width-540px">
                A quick and easy way to garner some extra promotion
                for your business, through your social media contacts.
                <br/>
                <a href="https://play.google.com/store/apps/details?id=com.reliantsoftwareeng.photologoadd">
                    See the app's listing in the Play Store
                </a>
            </p>
            <h4>Image selection and placement</h4>
            <p className="limit-width-540px">
                Select your logo image from your device,
                a photo you've taken, and where to add the logo
                in relation to the photo.
            </p>
            <img src={imagePaths.ma1} className="screenshot"/>
            <br/><br/>
            <h4>View the result</h4>
            <p>
                Then, if you like it, save it to your device.
            </p>
            <img src={imagePaths.ma2} className="screenshot"/>
            <br/><br/>
            <h4>Share the result</h4>
            <p className="limit-width-540px">
                Share the combined image with other apps on your device,
                including those for social media.
            </p>
            <img src={imagePaths.ma3} className="screenshot"/>
        </div>
        <div className="spacer-column"/>
    </div>;
