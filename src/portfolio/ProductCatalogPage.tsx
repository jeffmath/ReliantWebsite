import * as React from 'react'
import imagePaths from "./ImagePaths";

export const ProductCatalogPage = () =>
    <div className="flex">
        <div className="spacer-column"/>
        <div className="content-column">
            <h3>Product catalog example</h3>
            <p className="limit-width-750px">
                A traditional web application created using ASP.NET MVC 5,
                C#, Entity Framework, and SQL Server.
                It was built for a client who catalogs
                the educational aspects of various board games, as a way to
                promote playing such games as an alternative learning modality.
                The site employs a responsive design, as do most of the
                other apps in this portfolio.
            </p>
            <h4>Browsing of informative product summaries</h4>
            <p className="limit-width-750px">
                With filtering and paging designed to let the viewer quickly
                locate a particular board game among the hundreds
                catalogued in the database.
            </p>
            <img src={imagePaths.pc1} className="screenshot"/>
            <br/><br/>
            <h4>Multifaceted product details</h4>
            <p className="limit-width-750px">
                Designed to accommodate an open-ended amount of
                categorized information about each game.
            </p>
            <img src={imagePaths.pc2} className="screenshot"/>
            <br/><br/>
            <h4>Full featured search capability</h4>
            <p className="limit-width-750px">
                Enables searching for an arbitrary combination of
                game data criteria.
            </p>
            <img src={imagePaths.pc3} className="screenshot"/>
        </div>
        <div className="spacer-column"/>
    </div>;
