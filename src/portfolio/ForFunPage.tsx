import * as React from 'react'
import imagePaths from "./ImagePaths";

export const ForFunPage = () =>
    <div className="flex">
        <div className="spacer-column"/>
        <div className="content-column">
            <h3>For fun</h3>
            <p className="limit-width-640px">
                A game Jeff Mather created back in the mid 1990s, which he
                reimplemented for the web.
                You can play the game on its <a href="http://dunjax.com">website</a>.
            </p>
            <img src={imagePaths.ff1} className="screenshot"/>
            <br/><br/>
            <img src={imagePaths.ff2} className="screenshot"/>
            <br/><br/>
            <img src={imagePaths.ff3} className="screenshot"/>
            <br/><br/>
            <img src={imagePaths.ff4} className="screenshot"/>
        </div>
        <div className="spacer-column"/>
    </div>;
