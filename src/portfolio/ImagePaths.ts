
const imagePaths = {
    ba1: '/images/businessAutomation/masterDetails.jpg',
    ba2: '/images/businessAutomation/masterDetails2ndLevel.jpg',
    ba3: '/images/businessAutomation/reportGeneration.jpg',
    mi1: 'images/mapsIntegration/mapPage.jpg',
    mi2: 'images/mapsIntegration/editProfilePage.jpg',
    dg1: 'images/dataGathering/aggregation.jpg',
    dg2: 'images/dataGathering/focusedEditing.jpg',
    dg3: 'images/dataGathering/visit.jpg',
    om1: 'images/orderManagement/orders.jpg',
    om2: 'images/orderManagement/order.jpg',
    om3: 'images/orderManagement/products.jpg',
    pc1: 'images/productCatalog/browsingSummaries.jpg',
    pc2: 'images/productCatalog/productDetails.jpg',
    pc3: 'images/productCatalog/searchCapabilities.jpg',
    os1: 'images/onlineStore/availableTitles.jpg',
    os2: 'images/onlineStore/stripeIntegration.jpg',
    os3: 'images/onlineStore/adminOrder.jpg',
    ma1: 'images/photoLogoAdd/logoPlacement.jpg',
    ma2: 'images/photoLogoAdd/logoPlaced.jpg',
    ma3: 'images/photoLogoAdd/sharing.jpg',
    ff1: 'images/forFun/screenshot1.jpg',
    ff2: 'images/forFun/screenshot2.jpg',
    ff3: 'images/forFun/screenshot3.jpg',
    ff4: 'images/forFun/screenshot4.jpg',
};

export default imagePaths;