import * as React from 'react'
import imagePaths from './ImagePaths';

export const BusinessAutomationPage = () =>
    <div>
        <div className="flex margin-bottom-20px">
            <div className="spacer-column"/>
            <div className="content-column">
                <h3>Business automation example</h3>
                <p className="limit-width-750px">
                    A single page web application created using React.js,
                    Redux, Node.js, Express.js, and MongoDB.
                    This app replaced the client's ad-hoc Google Sheets solution,
                    which had become a management nightmare beyond a limited
                    number of jobs entered.
                </p>
                <p>You can view the <a href="https://gitlab.com/jeffmath/SuperiorPrint">GitLab repository</a> for this project.</p>
                <h4>Master/details editing</h4>
                <p className="limit-width-750px">
                    When a job is selected, its details and line items are displayed
                    for editing. Also, the navigation bar displays the various kinds
                    of entities which may be edited.
                </p>
                <img src={imagePaths.ba1} className="screenshot"/>
                <br/><br/>
                <h4>A second level of master/details editing</h4>
                <p>
                    When a job line item is selected, its details are displayed,
                    as well as options to generate reports from those details.
                </p>
                <img src={imagePaths.ba2} className="screenshot"/>
            </div>
            <div className="spacer-column"/>
        </div>
        <div className="flex">
            <div className="spacer-column"/>
            <div className="content-column">
                <h4>Automatic report generation</h4>
                <p>
                    The data from the edit forms are gathered in an intelligent
                    manner for presentation to the client (and its collaborators
                    and customers).
                </p>
                <img src={imagePaths.ba3} className="screenshot"/>
            </div>
            <div className="spacer-column"/>
        </div>
    </div>
;
