import * as React from 'react'
import imagePaths from "./ImagePaths";

export const OrderManagementPage = () =>
    <div className="flex">
        <div className="spacer-column"/>
        <div className="content-column">
            <h3>Order management example</h3>
            <p className="limit-width-900px">
                For this project, Reliant consulted for an
                entrepreneur who was creating a prototype system
                to present to potential investors.
                The system is a single page web application
                which employs AngularJS, Angular Material, ASP.NET Web API,
                Entity Framework, and SQL Server.  With the deadline
                of the presentation looming, Reliant assisted
                the entrepreneur in completing key functionalities
                he could demonstrate, such as those shown here.
            </p>
            <h4>Bringing order to orders</h4>
            <p>
                The system features an order management module.
            </p>
            <img src={imagePaths.om1} className="screenshot"/>
            <br/><br/>
            <h4>Order editing and tracking</h4>
            <p>
                This is how the edit page appears for an example order.
            </p>
            <img src={imagePaths.om2} className="screenshot"/>
            <br/><br/>
            <h4>Products</h4>
            <p>
                The system allows management of the products which may
                be ordered.
            </p>
            <img src={imagePaths.om3} className="screenshot"/>
        </div>
        <div className="spacer-column"/>
    </div>;
