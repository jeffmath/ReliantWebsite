import * as React from 'react'
import imagePaths from './ImagePaths';

export const MapsIntegrationPage = () =>
    <div className="flex">
        <div className="spacer-column"/>
        <div className="content-column">
            <h3>Google Maps integration example</h3>
            <p className="limit-width-750px">
                This is a single-page web application for social networking purposes.
                It was created using React.js and Redux on the front end to achieve
                a component-based, functional design.
                Play Framework, Scala, Quill, and PostgreSQL were employed
                on the back end to provide a stateless, reactive and highly scalable
                architecture.
            </p>
            <h4>Connect page</h4>
            <img src={imagePaths.mi1} className="screenshot"/>
            <br/><br/>
            <h4>Profile editing page</h4>
            <img src={imagePaths.mi2} className="screenshot"/>
        </div>
        <div className="spacer-column"/>
    </div>;
