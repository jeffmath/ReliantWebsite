import * as React from 'react'
import imagePaths from "./ImagePaths";

export const DataGatheringPage = () =>
    <div className="flex">
        <div className="spacer-column"/>
        <div className="content-column">
            <h3>Data gathering example</h3>
            <p className="limit-width-980px">
                A single page web application created using AngularJS,
                Node.js, Sails.js, and MySQL.
                Allows a hierarchy of staff members in the client organization
                to gather and manage the data they need to perform their
                specific roles.
            </p>
            <h4>Aggregation of collected data</h4>
            <p className="limit-width-980px">
                Data gathered by field workers is aggregated in multiple ways
                to permit quicker, easier management by supervisors.
            </p>
            <img src={imagePaths.dg1} className="screenshot"/>
            <br/><br/>
            <h4>Focused editing of large data sets</h4>
            <p className="limit-width-980px">
                A supervisor is presented with only the subset of the data
                which is relevant to his or her relationships within the
                organization.
            </p>
            <img src={imagePaths.dg2} className="screenshot"/>
            <br/><br/>
            <h4>Streamlined data entry for field workers</h4>
            <p className="limit-width-980px">
                A responsive design lets end users record data
                efficiently through their tablet, phone, or laptop.
            </p>
            <img src={imagePaths.dg3} className="screenshot"/>
        </div>
        <div className="spacer-column"/>
    </div>;
