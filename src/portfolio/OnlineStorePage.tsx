import * as React from 'react'
import imagePaths from "./ImagePaths";

export const OnlineStorePage = () =>
    <div className="flex">
        <div className="spacer-column"/>
        <div className="content-column">
            <h3>Online store example</h3>
            <p className="limit-width-980px">
                A single page web application (SPA) created using Angular,
                TypeScript, NgRx, Bootstrap, SASS, Stripe, Play Framework,
                Scala, and MongoDB. Created for an online video store operator
                whose legacy Classic ASP solution had become unserviceable.
                Included was conversion of the legacy Access database to
                MongoDB. Navigation through the app is as fast as if it were
                running on your machine, because as an SPA, it is!
            </p>
            <p>You can view the <a href="https://gitlab.com/DiscountVideo/play-version">GitLab repository</a> for this project.</p>
            <h4>Fast browsing of available titles</h4>
            <p className="limit-width-980px">
                There are thousands of titles available, but the user
                can swiftly page through (and filter) them, without
                having to reload the UI for every page of results.
            </p>
            <img src={imagePaths.os1} className="screenshot"/>
            <br/><br/>
            <h4>Stripe integration for payment processing</h4>
            <p className="limit-width-980px">
                Credit card information is never stored on client's server,
                yet the client may make as many charges as necessary on a
                customer's card.
            </p>
            <img src={imagePaths.os2} className="screenshot"/>
            <br/><br/>
            <h4>Separate administration site</h4>
            <p className="limit-width-980px">
                The site administrator may efficiently manage the data
                presented to the user, as well as orders and customer
                contact/shipping information.
            </p>
            <img src={imagePaths.os3} className="screenshot"/>
        </div>
        <div className="spacer-column"/>
    </div>;

