import * as React from 'react';
import {Link, NavLink} from 'react-router-dom';
import {Navbar, Nav, NavDropdown, MenuItem} from 'react-bootstrap';
const {LinkContainer} = require('react-router-bootstrap');

export const Navigation = () =>
    <Navbar style={{margin: 0}}>
        <Navbar.Header>
            <Navbar.Brand>
                <Link to="/" className="Alegreya">Reliant Software Engineering, LLC</Link>
            </Navbar.Brand>
        </Navbar.Header>
        <Nav>
            <NavDropdown title='Portfolio' id='portfolioDropdown'>
                <NavMenuItem link="mapsIntegration" name="Maps integration"/>
                <NavMenuItem link="businessAutomation" name="Business automation"/>
                <NavMenuItem link="onlineStore" name="Online store"/>
                <NavMenuItem link="dataGathering" name="Data gathering"/>
                <NavMenuItem link="orderManagement" name="Order management"/>
                <NavMenuItem link="productCatalog" name="Product catalog"/>
                <NavMenuItem link="mobileApp" name="Mobile app"/>
                <NavMenuItem link="forFun" name="For fun"/>
            </NavDropdown>
            <NavItem link="tools" name="Our toolset"/>
            <NavItem link="contact" name="Contact us"/>
        </Nav>
    </Navbar>;

const selectedStyle = {
    backgroundColor: "#e7e7e7"
};

interface NavBarItemProps
{
    link: string;
    name: string;
}

const NavItem = (props: NavBarItemProps) =>
    <li>
        <NavLink to={'/' + props.link} activeStyle={selectedStyle}>{props.name}</NavLink>
    </li>;

const NavMenuItem = (props: NavBarItemProps) =>
    <LinkContainer to={'/' + props.link}>
        <MenuItem>{props.name}</MenuItem>
    </LinkContainer>;
