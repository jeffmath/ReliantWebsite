const path = require('path');

module.exports = {
    entry: "main.tsx",
    mode: "production",
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: "bundle.js"
    },
    devtool: "source-map",
    resolve: {
        extensions: ['.ts', '.tsx', '.js'],
        modules: ['src', 'node_modules']
    },
    module: {
        rules: [
            { test: /\.tsx?$/, use: "ts-loader" },
            {
                test: /\.css$/,
                use: [
                    {loader: 'style-loader'},
                    {loader: 'postcss-loader'}
                ]
            },
            { test: /\.js$/, enforce: "pre", loader: "source-map-loader" }
        ]
    },
};